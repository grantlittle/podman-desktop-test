package me.grantlittle.demo.podmandesktoptest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.TestConfiguration;

@TestConfiguration(proxyBeanMethods = false)
public class TestPodmanDesktopTestApplication {

    public static void main(String[] args) {
        SpringApplication.from(PodmanDesktopTestApplication::main).with(TestPodmanDesktopTestApplication.class).run(args);
    }

}
