package me.grantlittle.demo.podmandesktoptest;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableAdminServer
@EnableJpaRepositories
public class PodmanDesktopTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(PodmanDesktopTestApplication.class, args);
    }

}
